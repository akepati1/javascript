window.onload = () => {

document.getElementById("div1").addEventListener('click', () => {
    alert('div1');
});

document.getElementById("div2").addEventListener('click', (e) => {
    alert('div2');
    // e.stopImmediatePropagation(); // Stops the multiple events having on same element.
    // e.preventDefault(); // stops the default behavior
});

document.getElementById("div2").addEventListener('click', (e) => {
    invoke();
});

function invoke() {
    alert("Hello World");
}

document.getElementById("div3").addEventListener('click', (e) => {
    alert('div3');
    e.stopPropagation(); // Stops its parents event handlers
});

var list = [ 'first', 'second'];
for( var i in list ) {
var li = document.createElement('li');
var textNode = document.createTextNode(list[i]);
var btn = document.createElement('button');
btn.innerHTML = "index";
console.log(i);
btn.onclick = function(i) {
alert('index is '+i);
}.bind(null, i);
// btn.onclick = function() {
//     alert('index is '+i);
//     };
li.appendChild(textNode);
li.appendChild(btn);
document.getElementById('root').appendChild(li);
}

}