  
class Modal{
  constructor(){
      this.items = []
      this.isSelectAll = false;
  }
  setItems = (items)=>{
      this.items = items
  }
  
}
class ItemGenarator{
    constructor(task,id){  
     let item =  {
        id:id,
        caption:task,
        isCompleted:false
     }
     return item;
    }
    isCompletedTask(){
        this.item.isCompleted = !this.item.isCompleted;
    }
}

class appModal {
    constructor(){
        this.itemsCollection = [],
        this.filter = 'all'
    }
    addItem(caption){
      
       this.itemsCollection.push(caption);
    // console.log(this.itemsCollection);
    }
}

let app = new appModal();
let store = new Modal();
/*----------Selectors------------- */


let input =document.getElementById('user_input');
console.log(input)
let all = document.getElementById('all');
let active = document.getElementById('active')
let count = document.getElementById('count');
let completed = document.getElementById('completed');
let clear   = document.getElementById('clear');
let selectAll = document.getElementById("selectAll");
let footer  = document.getElementById('footer');

/*---------------------View------------- */
let view = {
    addItem : (caption)=>{
         controller.addItem(caption);
    },
    render : (storage=store.items)=>{
        let id=0;
        let ul = document.getElementById('Tasks')
         ul.innerHTML='';
           
              
       for(let i=0;i<storage.length;i++){
          
           let list = document.createElement('li');
           list.setAttribute('id',`${id++}`);

           let div = document.createElement('div');
           div.setAttribute('class','view')

           let checkBox = document.createElement('div');
           checkBox.setAttribute('class','check')
          
           checkBox.addEventListener('click',()=>{
                 view.checkTheItem(storage[i].id)
           })
         
           
           let lable = document.createElement('lable');
           if(storage[i].isCompleted){
            //   checkBox.setAttribute('class','tick_mark')
             checkBox.classList.add('tick_mark')
             lable.classList.add('strick')
           }
           else{
               lable.removeAttribute('class')
           }
        
       
           
           lable.innerText = storage[i].caption;

           let deleteBtn = document.createElement('button');
      
           deleteBtn.setAttribute('class','destroy')
           deleteBtn.addEventListener('click',()=>{
               console.log(storage[i].id)
               view.deleteItem(storage[i].id);
           })
     
           let Items  = store.items.filter((ele)=>{
           
            return ele.isCompleted===false;
        })
        /* count of Items left */
        count.innerText = `${Items.length} Items Left`
        div.appendChild(checkBox)
    
           div.appendChild(lable);
           div.appendChild(deleteBtn);
           list.appendChild(div);
           ul.appendChild(list);   
        //    console.log(storage.length)      
         
       }
    //    console.log(store.items)
       if(store.items.length>0){
        footer.style.display="block";
        
    }  
    else if(store.items.length===0) {
        footer.style.display="none";
    }
    },
    checkTheItem : (id)=>{
       controller.checkBox(id);
    },
    deleteItem : (id)=>{
       controller.delete(id);
    },
    allItem: (event)=>{
          app.filter='all';
         controller.showAllItems();
           },
    completedItem : ()=>{
        app.filter='completed';
        controller.showCompletedItems();
       },
    activeItems : ()=>{
        app.filter='active';
        controller.showActiveItems();
    },
    clearAllCompleted : ()=>{
        // app.filter="";
        controller.clearItems();
    },
    selectAll : ()=>{
          app.filter="completed";
          controller.markAllCompleted();
    }
    
}
let id=0;

/*----------Controllera---------*/
let controller = {
    
    addItem: (caption)=>{
      let task = new ItemGenarator(caption,id++);
      app.addItem(task);
      store.setItems(app.itemsCollection)
    //   console.log(store.items)
    //   view.render(store.items);
      input.value=" "
      if(app.filter==='completed'){
        view.completedItem();
    }
    else if(app.filter==='active'){
        view.activeItems();
    }
    else{
        view.render()
    }
    },
    checkBox:(id)=>{
        console.log(store.items)
        let val  = store.items.find((ele)=>{
            return ele.id===id;
        })
        console.log(val)
        val.isCompleted = !val.isCompleted;
        if(app.filter==='completed'){
            view.completedItem();
        }
        else if(app.filter==='active'){
            view.activeItems();
        }
        else{
            view.render()
        }
       
    },
        delete:(index)=>{
        console.log(store.items[index])
         let delitems = store.items.filter((ele)=>{
             return ele.id!==index;
         })
         store.items=delitems;
         app.itemsCollection=store.items
       
         console.log(store.items)
         if(app.filter==='completed'){
             view.completedItem();
         }
         else if(app.filter==='active'){
             view.activeItems();
         }
         else{
             view.render()
         }
        
    
        },
        showAllItems : ()=>{
            view.render();
        } ,
        showCompletedItems : ()=>{
           let Items = store.items.filter((ele)=>{
                 
               return ele.isCompleted===true;
           })
          
           view.render(Items)
      },
      showActiveItems : ()=>{
        let Items = store.items.filter((ele)=>{
           
            return ele.isCompleted===false;
        })
     
        view.render(Items)
      },
      clearItems : ()=>{
        store.items =  store.items.filter((ele)=>{
           return ele.isCompleted===false;
         })
       
         app.itemsCollection=store.items
         console.log(app.items)
         if(app.filter==='completed'){
            view.completedItem();
            
        }
        else if(app.filter==='active'){
            view.activeItems();
        }
        else if(app.filter==='clear_completed'){
            view.completedItem();
        }
        else{
            view.render()
        }
       
      },
      markAllCompleted : ()=>{
          if(!store.isSelectAll){
              store.items.forEach((ele)=>{
                  return ele.isCompleted=true;
              })
              view.render();
              store.isSelectAll=true;
              console.log(store.items)
          }
          else {
            store.items.forEach((ele)=>{
                return ele.isCompleted=false;
            })
            view.render();
            store.isSelectAll=false
          }
      }

}
/*---------function----------*/
function func_focus(event){
    console.log(app.filter)
    console.log(event.target.innerText)
    if(app.filter===event.target.innerText){
      console.log('docus')
        event.target.setAttribute('class','border')
    }
   

}

/*------eventlisteners------- */

input.addEventListener('keypress',(event)=>{
      if(event.key==='Enter'){
          view.addItem(event.target.value)
        
      }
})
 all.addEventListener('click',(event)=>{
     view.allItem(event);
     func_focus(event)
     active.classList.remove('border')
     completed.classList.remove('border')
 })

 completed.addEventListener('click',(event)=>{
     view.completedItem(event);
     func_focus(event)
     active.classList.remove('border')
     all.classList.remove('border')
 })

 active.addEventListener('click',(event)=>{
    view.activeItems(event);
    func_focus(event)
    all.classList.remove('border')
     completed.classList.remove('border')
})
clear.addEventListener('click',()=>{
    view.clearAllCompleted();
})
selectAll.addEventListener("click",()=>{
      view.selectAll();
})

