/*----Global variables-----*/
let x=1;

/*---------Selectors-------*/
let tasks = document.getElementById('Tasks');
let input  = document.getElementsByTagName('input')[0];
let add = document.getElementById('btn');

/* ------------ functions---------*/
function addTask(){
    /*-----Validation----*/
    if(input.value===''){
        return;
    }

    /*------ create Elements-------*/
   
    let list = document.createElement('li');
    let textnode = document.createElement('span')
    textnode.innerText = input.value;
    let checkbox = document.createElement('input');
    checkbox.type='checkbox';
    checkbox.setAttribute('id',x++);
    checkbox.addEventListener('click',()=>{
       textnode.classList.toggle('strick')
    })
    let deleteBtn = document.createElement('button');
    deleteBtn.innerText = "Delete";
    deleteBtn.addEventListener('click',()=>{
        list.remove();
    })
    /*---- append created elements---*/

    list.append(checkbox);
    list.append(textnode);
    list.append(deleteBtn)
    tasks.append(list);
    input.value=""
 }

/*-------Event lisener-------*/
add.onclick = addTask;
input.addEventListener("keypress",(event)=>{
  
    if(event.key==="Enter"){
        addTask();
    }
    return;
})

/*---Selectors---*/

